// See https://aka.ms/new-console-template for more information
using AdventOfCodeCSharp.Days;
using AdventOfCodeCSharp.Models;

string directory = System.IO.Directory.GetCurrentDirectory();

DayOne dayOne = new DayOne("Day 1", directory + "/Data/day_01.txt");
DayTwo dayTwo = new DayTwo("Day 2", directory + "/Data/day_02.txt");
DayThree dayThree = new DayThree("Day 3", directory + "/Data/day_03.txt");
DayFour dayFour = new DayFour("Day 4", directory + "/Data/day_04.txt");
DayFive dayFive = new DayFive("Day 5", directory + "/Data/day_05.txt");
DaySix daySix = new DaySix("Day 6", directory + "/Data/day_06.txt");
DaySeven daySeven = new DaySeven("Day 7", directory + "/Data/day_07.txt");
DayEight dayEight = new DayEight("Day 8", directory + "/Data/day_08.txt");
DayNine dayNine = new DayNine("Day 9", directory + "/Data/day_09.txt");
DayTen dayTen = new DayTen("Day 10", directory + "/Data/day_10.txt");
DayEleven dayEleven = new DayEleven("Day 11", directory + "/Data/day_11.txt");
DayTwelve dayTwelve = new DayTwelve("Day 12", directory + "/Data/day_12.txt");
DayThirteen dayThirteen = new DayThirteen("Day 13", directory + "/Data/day_13.txt");
DayFourteen dayFourteen = new DayFourteen("Day 14", directory + "/Data/day_14.txt");

// Result(dayOne);
// Result(dayTwo);
// Result(dayThree);
// Result(dayFour);
// Result(dayFive);
// Result(daySix);
// Result(daySeven);
// Result(dayEight);
// Result(dayNine);
// Result(dayTen);
// Result(dayEleven);
// Result(dayTwelve);
// Result(dayThirteen);
Result(dayFourteen);


static void Result(Day day) {
    Console.WriteLine(day.name);
    Console.WriteLine("  Part 1: {0}", day.PartOne());
    Console.WriteLine("  Part 2: {0}", day.PartTwo());
}
