using System.Collections;
using System.Text.Json;
using System.Text.Json.Serialization;

namespace AdventOfCodeCSharp.Models {
    public class TreeNode<T> : IEnumerable {

        public TreeNode<T>? parent { get; set; }
        public List<TreeNode<T>> children { get; set; }
        public T? data { get; set; }

        public TreeNode(T? data = default(T), List<TreeNode<T>>? children = null) {
            if (data != null) {
                this.data = data;
            }
            if (children != null) {
                this.children = children;
            } else {
                this.children = new List<TreeNode<T>>();
            }
        }

        public TreeNode<T> AddChild(T child) {
            TreeNode<T> childNode = new TreeNode<T>(child) { parent = this };
            this.children.Add(childNode);
            return childNode;
        }

        public TreeNode<T> AddChild(TreeNode<T> child) {
            child.parent = this;
            this.children.Add(child);
            return child;
        }

        public bool Contains(T data) {
            foreach (TreeNode<T> node in this) {
                if (data.Equals(node.data)) {
                    return true;
                }
            }
            return false;
        }

        public TreeNode<T> Find(T data) {
            foreach (TreeNode<T> node in this) {
                if (data.Equals(node.data)) {
                    return node;
                }
            }
            return null;
        }

        public int GetParentsCount(Func<T, bool>? stopCondition = null) {
            if (this.parent != null && stopCondition != null) {
                if (stopCondition(this.parent.data)) {
                    this.parent = null;
                }
            }

            if (this.parent == null) {
                return 0;
            } else {
                return this.parent.GetParentsCount() + 1;
            }
        }

        IEnumerator IEnumerable.GetEnumerator() {
            return GetEnumerator();
        }

        public IEnumerator<TreeNode<T>> GetEnumerator() {
            yield return this;
            foreach (var directChild in this.children) {
                foreach (var anyChild in directChild)
                    yield return anyChild;
            }
        }
    }

    public class TreeNodeIntJsonConverter : JsonConverter<TreeNode<int>> {
        public override TreeNode<int> Read(ref Utf8JsonReader reader, Type typeToConvert, JsonSerializerOptions options) {
            switch (reader.TokenType) {
                case JsonTokenType.Number:
                    return new TreeNode<int>(reader.GetInt32(), null);
                case JsonTokenType.StartArray:
                    var tree = new TreeNode<int>(-1);
                    while (reader.Read()) {
                        if (reader.TokenType == JsonTokenType.EndArray)
                            break;
                        tree.AddChild(JsonSerializer.Deserialize<TreeNode<int>>(ref reader, options));
                    }
                    return tree;
                default:
                    throw new Exception("error");
            }
        }

        public override void Write(
            Utf8JsonWriter writer,
            TreeNode<int> objectToWrite,
            JsonSerializerOptions options) =>
            JsonSerializer.Serialize(writer, objectToWrite, objectToWrite.GetType(), options);
    }
}
