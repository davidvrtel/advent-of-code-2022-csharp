namespace AdventOfCodeCSharp.Models {
    public static class TupleExtensions {
        public static (int, int) Add(this (int, int) a, (int, int) b) => (a.Item1 + b.Item1, a.Item2 + b.Item2);
        public static (int, int) VectorTo(this (int, int) a, (int, int) b) => (b.Item1 - a.Item1, b.Item2 - a.Item2);
        public static double SquareSize(this (int, int) a) => Math.Pow(a.Item1, 2) + Math.Pow(a.Item2, 2);
    }
}
