namespace AdventOfCodeCSharp.Models {
    public class Input {
        public string data;

        public Input(string pathToDataFile) {
            data = ReadInputData(pathToDataFile);
        }

        private string ReadInputData(string path) {
            return System.IO.File.ReadAllText(path);
        }

    }
}
