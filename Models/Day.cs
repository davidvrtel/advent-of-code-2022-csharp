namespace AdventOfCodeCSharp.Models {
    public class Day : ISolution {
        public Input input;
        public string name;

        public Day(string nameOfDay, string pathToDataFile) {
            name = nameOfDay;
            input = new Input(pathToDataFile);
        }

        public virtual string PartOne() => "Not solved";
        public virtual string PartTwo() => "Not solved";
    }
}
