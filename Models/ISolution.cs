namespace AdventOfCodeCSharp.Models {
    public interface ISolution {
        string PartOne();
        string PartTwo();
    }
}
