using AdventOfCodeCSharp.Models;

namespace AdventOfCodeCSharp.Days {
    public class DayTwo : Day {
        private string[][] parsedInput;

        private IDictionary<string, RPS> choices = new Dictionary<string, RPS>(){
            {"A", RPS.Rock},
            {"B", RPS.Paper},
            {"C", RPS.Scissors},
            {"X", RPS.Rock},
            {"Y", RPS.Paper},
            {"Z", RPS.Scissors}
        };

        private IDictionary<string, Result> results = new Dictionary<string, Result>(){
            {"X", Result.Loss},
            {"Y", Result.Draw},
            {"Z", Result.Win}
        };

        private IDictionary<RPS, RPS> winnerLooserMap = new Dictionary<RPS, RPS>(){
            {RPS.Rock, RPS.Scissors},
            {RPS.Paper, RPS.Rock},
            {RPS.Scissors, RPS.Paper}
        };

        public DayTwo(string nameOfDay, string pathToDataFile) : base(nameOfDay, pathToDataFile) {
            parsedInput = ParseInput();
        }

        public override string PartOne() {
            return CalculatePlayerPoints().ToString();
        }

        public override string PartTwo() {
            return CalculatePlayerPoints2().ToString();
        }

        private int CalculatePlayerPoints() {

            return parsedInput.Aggregate(0, (acc, round) => {

                RPS playerChoice = choices[round[1]];
                RPS opponentChoice = choices[round[0]];

                int result = Convert.ToInt32(CompareRPS(playerChoice, opponentChoice));

                return acc + Convert.ToInt32(playerChoice) + result;
            });
        }

        private int CalculatePlayerPoints2() {
            return parsedInput.Aggregate(0, (acc, round) => {

                RPS opponentChoice = choices[round[0]];
                Result expectedResult = results[round[1]];
                RPS playerChoice = SelectPlayerChoice(opponentChoice, expectedResult);

                return acc + Convert.ToInt32(playerChoice) + Convert.ToInt32(expectedResult);
            });
        }

        private string[][] ParseInput() {
            return input.data.Split("\n").Select(round => round.Split(" ")).ToArray();
        }

        private Result CompareRPS(RPS first, RPS last) {
            if (first == last) {
                return Result.Draw;
            }

            if (last == winnerLooserMap[first]) {
                return Result.Win;
            } else {
                return Result.Loss;
            }
        }

        private RPS SelectPlayerChoice(RPS opponentsChoice, Result expectedResult) {
            switch (expectedResult) {
                case Result.Draw:
                    return opponentsChoice;
                case Result.Loss:
                    return winnerLooserMap[opponentsChoice];
                case Result.Win:
                    return winnerLooserMap.FirstOrDefault(x => x.Value == opponentsChoice).Key;
                default:
                    throw new Exception("No matched expected result");
            }
        }
    }

    public enum RPS //RockPaperScissors
    {
        Rock = 1,
        Paper = 2,
        Scissors = 3
    }

    public enum Result {
        Loss = 0,
        Draw = 3,
        Win = 6
    }

}
