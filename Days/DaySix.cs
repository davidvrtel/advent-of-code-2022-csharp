using AdventOfCodeCSharp.Models;

namespace AdventOfCodeCSharp.Days {
    public class DaySix : Day {

        public DaySix(string nameOfDay, string pathToDataFile) : base(nameOfDay, pathToDataFile) {

        }

        public override string PartOne() {
            // var watch1 = new System.Diagnostics.Stopwatch();
            // watch1.Start();
            // GetFirstDistinctCharMarker(4).ToString();
            // watch1.Stop();

            // var watch2 = new System.Diagnostics.Stopwatch();
            // watch2.Start();
            // GetFirstDistinctCharMarker2(4).ToString();
            // watch2.Stop();

            // Console.WriteLine("Time elapsed 1st (ms): {0}", watch1.Elapsed.TotalMilliseconds);
            // Console.WriteLine("Time elapsed 2nd (ms): {0}", watch2.Elapsed.TotalMilliseconds);

            return GetFirstDistinctCharMarker2(4).ToString();
        }

        public override string PartTwo() {
            // var watch1 = new System.Diagnostics.Stopwatch();
            // watch1.Start();
            // GetFirstDistinctCharMarker(14).ToString();
            // watch1.Stop();

            // var watch2 = new System.Diagnostics.Stopwatch();
            // watch2.Start();
            // GetFirstDistinctCharMarker2(14).ToString();
            // watch2.Stop();

            // Console.WriteLine("Time elapsed 1st (ms): {0}", watch1.Elapsed.TotalMilliseconds);
            // Console.WriteLine("Time elapsed 2nd (ms): {0}", watch2.Elapsed.TotalMilliseconds);

            return GetFirstDistinctCharMarker2(14).ToString();
        }

        private int GetFirstDistinctCharMarker(int chars) {
            for (int i = 0; i < (input.data.Length - chars); i++) {
                int charsPassed = i + chars;

                string chunk = input.data[i..charsPassed];

                if (chunk.Distinct().ToArray().Length == chars) {
                    return charsPassed;
                }
            }
            return -1;
        }

        private int GetFirstDistinctCharMarker2(int chars) {
            for (int i = 0; i < (input.data.Length - chars); i++) {
                int charsPassed = i + chars;

                string chunk = input.data[i..charsPassed];

                int firstDuplicatePosition = GetFirstDuplicateIndex(chunk);
                if (firstDuplicatePosition == -1) {
                    return charsPassed;
                } else {
                    i += firstDuplicatePosition;
                }
            }
            return -1;
        }

        private int GetFirstDuplicateIndex(string chunk) {
            for (int j = 0; j < chunk.Length - 1; j++) {
                for (int k = j + 1; k < chunk.Length; k++) {
                    if (chunk[j] == chunk[k]) {
                        return j;
                    }
                }
            }
            return -1;
        }
    }
}
