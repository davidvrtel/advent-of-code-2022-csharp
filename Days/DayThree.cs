using AdventOfCodeCSharp.Models;

namespace AdventOfCodeCSharp.Days {
    public class DayThree : Day {
        private string[][] parsedInput;

        public DayThree(string nameOfDay, string pathToDataFile) : base(nameOfDay, pathToDataFile) {
            parsedInput = ParseInput();
        }

        public override string PartOne() {
            return CalculateTotalPriority().ToString();
        }

        public override string PartTwo() {
            return CalculateTotalPriority2().ToString();
        }

        private string[][] ParseInput() {
            return input.data.Split("\n").Select(rucksack => {
                string first = rucksack[..(rucksack.Length / 2)];
                string second = rucksack[(rucksack.Length / 2)..(rucksack.Length)];
                string[] rucksackParts = { first, second };
                return rucksackParts;
            }).ToArray();
        }

        private int CalculateTotalPriority() {
            return parsedInput.Aggregate(0, (acc, rucksack) => {
                char[] first = rucksack[0].ToCharArray().Distinct().ToArray();
                char[] second = rucksack[1].ToCharArray().Distinct().ToArray();

                char sameItem = first.Intersect(second).First();

                return acc + GetPriority(sameItem);
            });

        }

        private int CalculateTotalPriority2() {
            return input.data.Split("\n")
                .Chunk(3)
                .Aggregate(0, (acc, group) => {
                    char[][] parsedGroup = group.Select(elf => elf.ToCharArray().Distinct().ToArray()).ToArray();

                    char sameItem = parsedGroup[0]
                                        .Intersect(parsedGroup[1])
                                        .ToArray()
                                        .Intersect(parsedGroup[2])
                                        .First();

                    return acc + GetPriority(sameItem);
                });

        }

        private int GetPriority(char item) {
            int code = (int)item;

            if (code > 96) {
                return code - 96;
            } else {
                return code - 38;
            }
        }
    }
}
