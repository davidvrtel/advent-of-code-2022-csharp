using AdventOfCodeCSharp.Models;

namespace AdventOfCodeCSharp.Days {
    public class DayFourteen : Day {

        private (int, int)[][] parsedInput;
        private List<(int, int)> rocks;
        private (int, int) start;
        private int lowestPoint;
        private int floorLevel;

        public DayFourteen(string nameOfDay, string pathToDataFile) : base(nameOfDay, pathToDataFile) {
            this.parsedInput = ParseInput();
            this.rocks = GenerateRocks();
            this.start = (500, 0);
            this.lowestPoint = rocks.Max(Comparer<(int, int)>.Create((x, y) => x.Item2.CompareTo(y.Item2))).Item2;
            this.floorLevel = lowestPoint + 2;
        }

        public override string PartOne() {
            return SimulateSand().ToString();
        }

        public override string PartTwo() {
            this.rocks = GenerateRocks();
            return SimulateSand(true).ToString();
        }

        private (int, int)[][] ParseInput() {
            return input.data.Split("\n").Select(line => line.Split(" -> ").Select(point => {
                string[] coordinates = point.Split(",");
                return (int.Parse(coordinates[0]), int.Parse(coordinates[1]));
            }).ToArray()).ToArray();
        }

        private int SimulateSand(bool isFloor = false) {
            int restedCount = 0;
            bool run = true;

            while (run) {
                (int, int) unit = start;
                bool rested = false;

                while (!rested) {
                    if (isFloor && unit.Item2 == floorLevel - 1) {
                        rocks.Add(unit);
                        restedCount++;
                        rested = true;
                    }

                    (int, int) newPosition = (unit.Item1, unit.Item2 + 1);
                    if (!isFloor && newPosition.Item2 > lowestPoint) {
                        run = false;
                        break;
                    }

                    if (!rocks.Contains(newPosition)) {
                        unit = newPosition;
                        continue;
                    }

                    newPosition.Item1 = newPosition.Item1 - 1;
                    if (!rocks.Contains(newPosition)) {
                        unit = newPosition;
                        continue;
                    }

                    newPosition.Item1 = newPosition.Item1 + 2;
                    if (!rocks.Contains(newPosition)) {
                        unit = newPosition;
                        continue;
                    }

                    if (unit == start) {
                        rocks.Add(unit);
                        restedCount++;
                        rested = true;
                        run = false;
                        break;
                    }

                    rocks.Add(unit);
                    restedCount++;
                    rested = true;
                }
            }

            return restedCount;
        }

        private List<(int, int)> GenerateRocks() {
            return parsedInput.SelectMany(rock => GenerateRockPoints(rock)).Distinct().ToList();
        }

        private (int, int)[] GenerateRockPoints((int, int)[] rock) {
            (int, int)[] points = new (int, int)[] { };
            for (int i = 0; i < rock.Length - 1; i++) {
                (int, int) start = rock[i];
                (int, int) end = rock[i + 1];

                if (start.Item1 == end.Item1) {
                    points = points.Concat(GenerateRange((false, true), start.Item1, (start.Item2, end.Item2))).ToArray();
                    continue;
                }

                if (start.Item2 == end.Item2) {
                    points = points.Concat(GenerateRange((true, false), start.Item2, (start.Item1, end.Item1))).ToArray();
                    continue;
                }
            }

            return points.Distinct().ToArray();
        }

        private (int, int)[] GenerateRange((bool, bool) map, int constant, (int, int) limits) {
            int count = Math.Abs(limits.Item1 - limits.Item2) + 1;
            int start = limits.Item1 > limits.Item2 ? limits.Item2 : limits.Item1;

            return map switch {
                (true, false) => Enumerable.Range(start, count).Select(item => (item, constant)).ToArray(),
                (false, true) => Enumerable.Range(start, count).Select(item => (constant, item)).ToArray(),
                _ => throw new Exception("no match in range map")
            };
        }
    }
}
