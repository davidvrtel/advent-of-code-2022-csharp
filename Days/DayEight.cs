using AdventOfCodeCSharp.Models;

namespace AdventOfCodeCSharp.Days {
    public class DayEight : Day {

        private int[][] parsedInput;

        public DayEight(string nameOfDay, string pathToDataFile) : base(nameOfDay, pathToDataFile) {
            parsedInput = ParseInput();
        }

        public override string PartOne() {
            return GetVisibleTreesCount().ToString();
        }

        public override string PartTwo() {
            return GetTreeScore().ToString();
        }

        private int[][] ParseInput() {
            return input.data.Split("\n")
                .Select(line => line.ToArray()
                    .Select(character => int.Parse(character.ToString()))
                    .ToArray())
                .ToArray();
        }

        private int GetTreeScore() {
            int padding = 1; // margin from sides for iterating and checking trees, start from index 1 and end 1 earlier
            (int, int) dimensions = (parsedInput.Length, parsedInput[0].Length); // (rows, columns)

            int count = 0;

            for (int i = padding; i < dimensions.Item1 - padding; i++) {
                for (int j = padding; j < dimensions.Item2 - padding; j++) {
                    int tree = parsedInput[i][j];

                    int[] left = parsedInput[i][..j].Reverse().ToArray();
                    int[] right = parsedInput[i][(j + 1)..];

                    int[] column = parsedInput.Select(row => row[j]).ToArray();
                    int[] up = column[..i].Reverse().ToArray();
                    int[] down = column[(i + 1)..];

                    int score = TreesSeenInDirection(tree, left) *
                                TreesSeenInDirection(tree, right) *
                                TreesSeenInDirection(tree, up) *
                                TreesSeenInDirection(tree, down);

                    if (score > count) {
                        count = score;
                    }
                }
            }

            return count;
        }

        private int TreesSeenInDirection(int height, int[] direction) {
            int index = Array.FindIndex(direction, tree => tree >= height);
            return index == -1 ? direction.Length : index + 1;
        }

        private int GetVisibleTreesCount() {
            int padding = 1; // margin from sides for iterating and checking trees, start from index 1 and end 1 earlier
            (int, int) dimensions = (parsedInput.Length, parsedInput[0].Length); // (rows, columns)

            int count = 2 * (dimensions.Item1 - 1) + 2 * (dimensions.Item2 - 1);

            for (int i = padding; i < dimensions.Item1 - padding; i++) {
                for (int j = padding; j < dimensions.Item2 - padding; j++) {
                    int tree = parsedInput[i][j];

                    int[] left = parsedInput[i][..j];
                    int[] right = parsedInput[i][(j + 1)..];

                    int[] column = parsedInput.Select(row => row[j]).ToArray();
                    int[] up = column[..i];
                    int[] down = column[(i + 1)..];

                    if (left.All(item => item < tree) ||
                        right.All(item => item < tree) ||
                        up.All(item => item < tree) ||
                        down.All(item => item < tree)) {
                        count++;
                    }
                }
            }

            return count;
        }

    }
}
