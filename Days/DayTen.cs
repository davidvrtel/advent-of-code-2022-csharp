using AdventOfCodeCSharp.Models;

namespace AdventOfCodeCSharp.Days {

    public class DayTen : Day {

        private string[] parsedInput;
        private List<char> display;

        public DayTen(string nameOfDay, string pathToDataFile) : base(nameOfDay, pathToDataFile) {
            parsedInput = ParseInput();
            display = new List<char>() { '\n' };
        }

        public override string PartOne() {
            return CountStrength().ToString();
        }

        public override string PartTwo() {
            return GetDisplay();
        }

        public string[] ParseInput() {
            return input.data.Split("\n");
        }

        public string GetDisplay() {
            return string.Join("", display);
        }

        public int CountStrength() {
            int registerX = 1;
            int strength = 0;
            int cycle = 0;

            int[] spritePosition(int register) => new int[3] { register - 1, register, register + 1 };

            foreach (string input in parsedInput) {
                (string, int, int) instruction = input[..4] switch {
                    "noop" => ("noop", 0, 1),
                    "addx" => ("addx", int.Parse(input[5..]), 2),
                    _ => throw new ArgumentException("no operation matched")
                };

                int[] sprite = spritePosition(registerX);

                for (int i = 0; i < instruction.Item3; i++) {
                    cycle++;
                    int cycleInRow = cycle % 40;

                    char pixel = sprite.Contains(cycleInRow - 1) ? '#' : '.';
                    display.Add(pixel);

                    strength += SignalStrength(cycle, registerX);
                    if (cycleInRow == 0) {
                        display.Add('\n');
                    }
                }

                registerX += instruction.Item2;
            }

            return strength;
        }

        private int SignalStrength(int cycle, int register) {
            if (cycle == 20 || (cycle - 20) % 40 == 0) {
                return cycle * register;
            } else {
                return 0;
            }
        }
    }
}
