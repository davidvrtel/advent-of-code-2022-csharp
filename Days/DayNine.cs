using AdventOfCodeCSharp.Models;

namespace AdventOfCodeCSharp.Days {
    public class DayNine : Day {

        private readonly (int, int) startingPosition = (0, 0);
        // private (int, int) headPosition;
        // private (int, int) tailPosition;
        private (int, int)[] knots = { };

        private (int, int, int)[] parsedInput; // (x, y, value)
        private HashSet<(int, int)> visitedPositions = new HashSet<(int, int)>();

        public DayNine(string nameOfDay, string pathToDataFile) : base(nameOfDay, pathToDataFile) {
            parsedInput = ParseInput();
        }

        public override string PartOne() {
            knots = Enumerable.Repeat(startingPosition, 2).ToArray();
            return GetVisitedPositionsCount().ToString();
        }

        public override string PartTwo() {
            visitedPositions = new HashSet<(int, int)>();
            knots = Enumerable.Repeat(startingPosition, 10).ToArray();
            return GetVisitedPositionsCount().ToString();
        }

        private (int, int, int)[] ParseInput() {
            return input.data.Split("\n").Select(line => {
                char direction = line[0];
                int value = int.Parse(line[2..]);

                return GetInputVector(direction, value);
            }).ToArray();
        }

        private (int, int, int) GetInputVector(char direction, int value) =>
            direction switch {
                'L' => (-1, 0, value),
                'R' => (1, 0, value),
                'U' => (0, 1, value),
                'D' => (0, -1, value),
                _ => throw new ArgumentOutOfRangeException("No known direction.")
            };

        private int GetVisitedPositionsCount() {
            MoveRope();
            return visitedPositions.Count();
        }

        private void MoveRope() {

            foreach ((int, int, int) input in parsedInput) {
                (int, int) unitVector = (input.Item1, input.Item2);
                int value = input.Item3;
                for (int i = 0; i < value; i++) {
                    (int, int) lastLeadingPosition = knots.First();
                    knots[0] = lastLeadingPosition.Add(unitVector);
                    // Visualize();

                    for (int j = 1; j < knots.Length; j++) {
                        bool moveOrder = IsFollowingKnotMoving(knots[j - 1], knots[j]);

                        if (moveOrder == true) {
                            (int, int) distanceVector = knots[j].VectorTo(knots[j - 1]);
                            int x = distanceVector.Item1;
                            int y = distanceVector.Item2;
                            (int, int) directionVector = (
                                    x == 0 ? 0 : x / Math.Abs(x),
                                    y == 0 ? 0 : y / Math.Abs(y)
                                );

                            (int, int) lastFollowingPosition = knots[j];
                            knots[j] = knots[j].Add(directionVector);
                            lastLeadingPosition = lastFollowingPosition;

                        } else {
                            break;
                        }
                    }

                    visitedPositions.Add(knots.Last());
                }
            }
        }

        private bool IsFollowingKnotMoving((int, int) leadingKnot, (int, int) followingKnot) {
            (int, int) distanceVector = followingKnot.VectorTo(leadingKnot);

            switch (distanceVector.SquareSize()) {
                case 0d:
                    return false;
                case 1d:
                    return false;
                case 2d:
                    return false;
                case 4d:
                    return true;
                case 5d:
                    return true;
                case 8d:
                    return true;
                default:
                    throw new ArgumentException("Unmatched distance between two points found.");
            }
        }

        private void Visualize() {
            string[,] plane = new string[60, 60];

            foreach ((int, int) knot in knots) {
                plane[knot.Item1 + 29, knot.Item2 + 29] = "X";
            }

            for (int i = 59; i >= 0; i--) {
                string line = "";
                for (int j = 0; j < 60; j++) {
                    if (plane[j, i] == "X") {
                        line += " X";
                    } else {
                        line += " O";
                    }
                }
                Console.WriteLine(line);
                line = "";
            }
            Console.WriteLine("line");
        }
    }
}
