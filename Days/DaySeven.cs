using System.Linq;

using AdventOfCodeCSharp.Models;

namespace AdventOfCodeCSharp.Days {
    public class DaySeven : Day {
        private string[] parsedInput;
        private List<string> workingDirectory;
        private SortedDictionary<string[], int> directoryDict;

        public DaySeven(string nameOfDay, string pathToDataFile) : base(nameOfDay, pathToDataFile) {
            parsedInput = ParseInput();
            workingDirectory = new List<string>();
            directoryDict = new SortedDictionary<string[], int>(Comparer<string[]>.Create((x, y) => string.Join("/", y).CompareTo(string.Join("/", x))));
        }

        public override string PartOne() {
            return GetLargeFoldersSum().ToString();
        }

        public override string PartTwo() {
            return GetDirectoryToDelete().ToString();
        }

        private string[] ParseInput() {
            return input.data.Split("\n");
        }

        private int GetDirectoryToDelete() {
            int totalSpace = 70000000;
            int neededSpace = 30000000;
            int usedSpace = directoryDict.ElementAt(directoryDict.Count - 1).Value;
            int leastDirectorySize = neededSpace - (totalSpace - usedSpace);

            int closestSize = neededSpace;

            foreach (KeyValuePair<string[], int> directory in directoryDict) {
                int size = directory.Value;

                if (size >= leastDirectorySize && size < closestSize) {
                    closestSize = size;
                }
            }

            return closestSize;
        }

        private int GetLargeFoldersSum() {
            SortDirectoryStructure();

            return directoryDict.Aggregate(0, (acc, directory) => {
                if (directory.Value <= 100000) {
                    return acc + directory.Value;
                } else {
                    return acc;
                }
            });
        }

        private void SortDirectoryStructure() {
            GetDirectoryStructure();

            for (int i = 0; i < directoryDict.Count; i++) {
                KeyValuePair<string[], int> directory = directoryDict.ElementAt(i);
                if (directory.Key.Length == 1) {
                    continue;
                } else {
                    var parent = directory.Key.SkipLast(1).ToArray();
                    directoryDict[parent] += directoryDict[directory.Key];
                }
            }
        }

        private void GetDirectoryStructure() {
            int parsedInputLength = parsedInput.Length;

            for (int i = 0; i < parsedInputLength; i++) {
                string line = parsedInput[i];

                switch (line[0..3]) {
                    case "$ c":
                        string direction = line[5..];
                        if (direction == "..") {
                            workingDirectory.RemoveAt(workingDirectory.Count - 1);
                            break;
                        } else {
                            workingDirectory.Add(direction);
                            break;
                        }

                    case "$ l":
                        int size = 0;

                        for (int j = i + 1; j < parsedInputLength; j++) {
                            string item = parsedInput[j];

                            if (!item.StartsWith("dir") && !item.StartsWith("$")) {
                                size += int.Parse(item.Split(" ")[0]);
                            } else if (item.StartsWith("$")) {
                                directoryDict.Add(workingDirectory.ToArray(), size);
                                i = j - 1;
                                break;
                            } else {
                                continue;
                            }

                            if ((j + 1) == parsedInputLength) {
                                directoryDict.Add(workingDirectory.ToArray(), size);
                                return;
                            }
                        }

                        break;
                    default:
                        throw new FormatException("Didn't match any command.");

                }
            }
        }
    }
}
