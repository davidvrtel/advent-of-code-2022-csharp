using System.Text.RegularExpressions;

using AdventOfCodeCSharp.Models;

namespace AdventOfCodeCSharp.Days {
    public class DayEleven : Day {

        private Monke[] monkas;

        public DayEleven(string nameOfDay, string pathToDataFile) : base(nameOfDay, pathToDataFile) {
            monkas = ParseInput();
        }

        public override string PartOne() {
            return CountInspections(20, 3).ToString();
        }

        public override string PartTwo() {
            monkas = ParseInput();
            int divisor = monkas.Aggregate(1, (acc, monke) => acc * monke.divisor);
            return CountInspections(10000, divisor).ToString();
        }

        private long CountInspections(int rounds, int divisor) {
            monkas = monkas.OrderBy(monke => monke.order).ToArray();
            for (int i = 0; i < rounds; i++) {
                PlayRound(divisor);
            }
            monkas = monkas.OrderByDescending(monke => monke.inspections).ToArray();

            return (long)monkas[0].inspections * (long)monkas[1].inspections;
        }

        private void PlayRound(int divisor) {
            for (int i = 0; i < monkas.Length; i++) {
                monkas[i].InspectItems(divisor);
                for (int j = 0; j < monkas[i].items.Count; j++) {
                    long receiving = this.monkas[i].Test(monkas[i].items[j]);
                    this.monkas[receiving].ReceiveItem(monkas[i].items[j]);
                }
                monkas[i].items.Clear();
            }
        }

        private Monke[] ParseInput() {
            return input.data.Split("\n\n")
                .Select(m => {
                    string[] lines = m.Split("\n");

                    int order = int.Parse(Regex.Matches(lines[0], @"\d+").First().Value);
                    List<long> items = Regex.Matches(lines[1], @"\d+").ToList().ConvertAll(item => long.Parse(item.Value));

                    string[] raw = lines[2][19..].Split(" ");
                    (string, string, string) operation = (raw[0], raw[1], raw[2]);

                    int divisor = int.Parse(Regex.Matches(lines[3], @"\d+").First().Value);
                    int firstTarget = int.Parse(Regex.Matches(lines[4], @"\d+").First().Value);
                    int secondTarget = int.Parse(Regex.Matches(lines[5], @"\d+").First().Value);
                    (int, int, int) test = (divisor, firstTarget, secondTarget);

                    return new Monke(order, items, operation, test);
                }).ToArray();
        }
    }

    public class Monke {

        public int order;
        public int divisor;
        public List<long> items { get; set; }

        public int inspections = 0;

        private Func<long, long> Operation;
        public Func<long, long> Test;

        public Monke(int order, List<long> items, (string, string, string) operation, (int, int, int) test) {
            this.order = order;
            this.items = items;
            this.Operation = operation switch {
                ("old", "*", "old") => (long old) => (long)(old * old),
                ("old", "+", "old") => (long old) => (long)(old + old),
                ("old", "*", string num) => (long old) => (long)(old * int.Parse(num)),
                ("old", "+", string num) => (long old) => (long)(old + int.Parse(num)),
                _ => throw new Exception("unexpected operation")
            };
            this.divisor = test.Item1;
            this.Test = (long item) => {
                long modulo = item % (long)test.Item1;
                if (modulo == 0) {
                    return test.Item2;
                } else {
                    return test.Item3;
                }
            };
        }

        public void InspectItems(int divisor) {
            items = items.Select(item => {
                long result = Operation(item);

                if (divisor == 3) {
                    double tmp = result / (long)divisor;
                    result = (long)Math.Floor(tmp);
                } else {
                    result = result % (long)divisor;
                }

                inspections++;
                return result;

            }).ToList();
        }

        public void ReceiveItem(long receivedItem) {
            items.Add(receivedItem);
        }
    }
}
