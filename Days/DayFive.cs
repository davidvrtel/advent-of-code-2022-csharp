using System.Text.RegularExpressions;
using System.Collections.Generic;

using AdventOfCodeCSharp.Models;
using System.ComponentModel;

namespace AdventOfCodeCSharp.Days {
    public class DayFive : Day {

        private IDictionary<int, List<char>> crateStacks;
        private readonly int[][] instructions;

        public DayFive(string nameOfDay, string pathToDataFile) : base(nameOfDay, pathToDataFile) {
            string[] splitInput = SplitInputByEmptyLine();
            instructions = ParseInstructions(splitInput.Last());
            crateStacks = ParseCratesMap(splitInput.First());
        }

        public override string PartOne() {
            MoveCrates(true);
            return GetTopCrates();
        }

        public override string PartTwo() {
            string[] splitInput = SplitInputByEmptyLine();
            crateStacks = ParseCratesMap(splitInput.First());
            MoveCrates();
            return GetTopCrates();
        }

        private string GetTopCrates() {
            return crateStacks.Aggregate("", (acc, stack) => acc + stack.Value.Last());
        }

        private void MoveCrates(bool byOne = false) {
            foreach (int[] instruction in instructions) {
                int count = instruction[0];
                int origin = instruction[1];
                int destination = instruction[2];

                List<char> originList = crateStacks[origin];
                List<char> destinationList = crateStacks[destination];

                List<char> translatedPart = originList.TakeLast(count).ToList();

                if (byOne) {
                    translatedPart.Reverse();
                }

                destinationList.AddRange(translatedPart);
                originList.RemoveRange(originList.Count - count, count);
            }
        }

        private IDictionary<int, List<char>> ParseCratesMap(string cratesMap) {
            string[] parsedCratesMap = cratesMap.Split("\n");

            string[] crates = parsedCratesMap.SkipLast(1).Reverse().ToArray();

            int[] stacks = Regex.Matches(parsedCratesMap.Last(), @"\d+")
                            .Select(num => int.Parse(num.Value))
                            .ToArray();

            IDictionary<int, List<char>> result = new Dictionary<int, List<char>>();

            foreach (int stack in stacks) {
                List<char> crateStack = new List<char>();

                foreach (string row in crates) {
                    char crate = row[1 + (4 * (stack - 1))];
                    if (crate != ' ') {
                        crateStack.Add(crate);
                    }
                }

                result.Add(stack, crateStack);
            }

            return result;
        }

        private int[][] ParseInstructions(string instructions) {
            return instructions.Split("\n")
                .Select(instruction =>
                    Regex.Matches(instruction, @"\d+")
                        .Select(num => int.Parse(num.Value))
                        .ToArray())
                .ToArray();
        }

        private string[] SplitInputByEmptyLine() {
            return input.data.Split("\n\n");
        }
    }
}

