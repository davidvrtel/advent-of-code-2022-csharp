using System.Text.Json;
using AdventOfCodeCSharp.Models;

namespace AdventOfCodeCSharp.Days {
    public class DayThirteen : Day {

        private TreeNode<int>[][] parsedInput;

        public DayThirteen(string nameOfDay, string pathToDataFile) : base(nameOfDay, pathToDataFile) {
            this.parsedInput = ParseInput();
        }

        public override string PartOne() {
            return Compare().ToString();
        }

        public override string PartTwo() {
            return Sort().ToString();
        }

        private int Sort() {
            TreeNode<int>[] flatInput = parsedInput.SelectMany(item => item).ToArray();
            TreeNode<int> firstDivider = ParseList("[[2]]");
            TreeNode<int> secondDivider = ParseList("[[6]]");
            flatInput = flatInput.Append(firstDivider).Append(secondDivider).ToArray();

            List<TreeNode<int>> flatList = flatInput.OrderBy(
                    tree => tree,
                    Comparer<TreeNode<int>>.Create((x, y) => CompareLists(x.children, y.children)))
                .ToList();

            int first = flatList.FindIndex(item => item == firstDivider) + 1;
            int second = flatList.FindIndex(item => item == secondDivider) + 1;

            return first * second;
        }

        private int Compare() {
            List<int> indices = new List<int>();

            for (int i = 0; i < parsedInput.Length; i++) {
                TreeNode<int> left = parsedInput[i][0];
                TreeNode<int> right = parsedInput[i][1];

                int result = CompareLists(left.children, right.children);

                if (result == -1) {
                    indices.Add(i + 1);
                }

            }
            return indices.Sum();
        }

        private int CompareLists(List<TreeNode<int>> left, List<TreeNode<int>> right) {

            for (int i = 0; i < left.Count; i++) {
                if (i + 1 > right.Count) {
                    return 1;
                }

                int leftValue = left[i].data;
                int rightValue = right[i].data;

                if (leftValue > -1 && rightValue > -1) {
                    int result = CompareIntegers(leftValue, rightValue);
                    if (result == 0) {
                        continue;
                    } else {
                        return result;
                    }
                }

                List<TreeNode<int>> leftList = left[i].children;
                List<TreeNode<int>> rightList = right[i].children;

                if (leftValue == -1 && rightValue == -1) {
                    int result = CompareLists(leftList, rightList);
                    if (result == 0) {
                        continue;
                    } else {
                        return result;
                    }
                }

                if (leftValue == -1 && rightValue > -1) {
                    List<TreeNode<int>> converted = new List<TreeNode<int>>() { right[i] };

                    int result = CompareLists(leftList, converted);
                    if (result == 0) {
                        continue;
                    } else {
                        return result;
                    }
                }

                if (leftValue > -1 && rightValue == -1) {
                    List<TreeNode<int>> converted = new List<TreeNode<int>>() { left[i] };

                    int result = CompareLists(converted, rightList);
                    if (result == 0) {
                        continue;
                    } else {
                        return result;
                    }
                }
            }

            if (left.Count < right.Count) {
                return -1;
            } else {
                return 0;
            }
        }

        private int CompareIntegers(int left, int right) {
            if (left == right) {
                return 0;
            } else if (left > right) {
                return 1;
            } else {
                return -1;
            }
        }

        private TreeNode<int>[][] ParseInput() {
            return input.data.Split("\n\n")
                .Select(pair => pair.Split("\n")
                    .Select(list => ParseList(list))
                    .ToArray())
                .ToArray();
        }

        TreeNode<int> ParseList(string list) {
            var options = new JsonSerializerOptions();
            options.WriteIndented = true;
            options.Converters.Add(new TreeNodeIntJsonConverter());
            return JsonSerializer.Deserialize<TreeNode<int>>(list, options)!;
        }
    }
}
