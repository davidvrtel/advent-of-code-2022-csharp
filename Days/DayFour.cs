using AdventOfCodeCSharp.Models;

namespace AdventOfCodeCSharp.Days {
    public class DayFour : Day {

        private int[][][] parsedInput;

        public DayFour(string nameOfDay, string pathToDataFile) : base(nameOfDay, pathToDataFile) {
            parsedInput = GetParsedCouples();
        }

        public override string PartOne() {
            return GetSectorFullOverlap().ToString();
        }

        public override string PartTwo() {
            return GetSectorPartialOverlap().ToString();
        }

        private int GetSectorFullOverlap() {
            return parsedInput
                .Aggregate(0, (acc, couple) => {
                    if ((couple[0][0] <= couple[1][0] && couple[0][1] >= couple[1][1]) ||
                        (couple[0][0] >= couple[1][0] && couple[0][1] <= couple[1][1])) {
                        return acc + 1;
                    } else {
                        return acc;
                    }
                });
        }

        private int GetSectorPartialOverlap() {
            return parsedInput
                .Aggregate(0, (acc, couple) => {
                    int[] first = Enumerable.Range(couple[0][0], couple[0][1] - couple[0][0] + 1).ToArray();
                    int[] second = Enumerable.Range(couple[1][0], couple[1][1] - couple[1][0] + 1).ToArray();

                    if (first.Intersect(second).ToArray().Length > 0) {
                        return acc + 1;
                    } else {
                        return acc;
                    }
                });
        }

        private int[][][] GetParsedCouples() {
            return input.data.Split("\n")
                .Select(couple => couple.Split(",")
                    .Select(sector => sector.Split("-").ToList()
                        .ConvertAll(boundary => {
                            int a = 0;
                            int.TryParse(boundary, out a);
                            return a;
                        })
                        .ToArray())
                    .ToArray())
                .ToArray();
        }
    }
}
