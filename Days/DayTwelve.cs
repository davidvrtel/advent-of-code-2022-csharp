using AdventOfCodeCSharp.Models;

namespace AdventOfCodeCSharp.Days {
    public class DayTwelve : Day {

        private int[,] heightMap;
        private readonly int yMax;
        private readonly int xMax;

        private (int, int) start;
        private (int, int) end;


        public DayTwelve(string nameOfDay, string pathToDataFile) : base(nameOfDay, pathToDataFile) {
            heightMap = ParseInput();
            yMax = heightMap.GetUpperBound(0);
            xMax = heightMap.GetUpperBound(1);
        }

        public override string PartOne() {
            // Visualize();
            return CountSteps((start.Item1, start.Item2, Height('a')), (end.Item1, end.Item2, Height('z'))).ToString();
        }

        public override string PartTwo() {
            return CountSteps((end.Item1, end.Item2, Height('z')), (-1, -1, Height('a'))).ToString();
        }

        private void Visualize() {
            string output = "";

            for (int iy = 0; iy <= yMax; iy++) {
                for (int ix = 0; ix <= xMax; ix++) {
                    string pad = heightMap[iy, ix] < 10 ? "  " : " ";
                    output += heightMap[iy, ix] + pad;
                }
                output += "\n";
            }

            Console.Write(output);
        }

        private int CountSteps((int, int, int) startPos, (int, int, int) endPos) {
            TreeNode<(int, int, int)> endNode = FindPaths(startPos, endPos);

            if (endNode == null) {
                return -1;
            } else {
                return endNode.GetParentsCount();
            }
        }

        private TreeNode<(int, int, int)> FindPaths((int, int, int) startPos, (int, int, int) endPos) {
            TreeNode<(int, int, int)> paths = new TreeNode<(int, int, int)>(startPos);
            List<TreeNode<(int, int, int)>> buffer = new List<TreeNode<(int, int, int)>>() { paths };

            while (buffer.Count > 0) {
                List<TreeNode<(int, int, int)>> newBuffer = new List<TreeNode<(int, int, int)>>();

                for (int i = 0; i < buffer.Count; i++) {
                    List<(int, int, int)> adjacent = GetAdjacent(buffer[i].data);
                    adjacent = adjacent.Where(spot => {

                        if (endPos.Item1 == -1 || endPos.Item2 == -1) {
                            return spot.Item3 >= buffer[i].data.Item3 - 1 && !paths.Contains(spot);
                        } else {
                            return spot.Item3 <= buffer[i].data.Item3 + 1 && !paths.Contains(spot);
                        }
                    }).ToList();


                    foreach ((int, int, int) spot in adjacent) {
                        TreeNode<(int, int, int)> newNode = buffer[i].AddChild(spot);

                        if ((endPos.Item1 == -1 || endPos.Item2 == -1) && spot.Item3 == endPos.Item3) {
                            return newNode;
                        }

                        if (spot == endPos) {
                            return newNode;
                        }

                        newBuffer.Add(newNode);
                    }
                }
                buffer = newBuffer;
            }

            return null;
        }

        private int[,] ParseInput() {
            string[] lines = input.data.Split("\n");
            int x = lines[0].Length;
            int y = lines.Length;

            int[,] parsedInput = new int[y, x];

            for (int iy = 0; iy < y; iy++) {
                for (int ix = 0; ix < x; ix++) {
                    char mark = lines[iy][ix];
                    switch (mark) {
                        case 'S':
                            parsedInput[iy, ix] = Height('a');
                            start = (iy, ix);
                            break;
                        case 'E':
                            parsedInput[iy, ix] = Height('z');
                            end = (iy, ix);
                            break;
                        default:
                            parsedInput[iy, ix] = Height(mark);
                            break;
                    }
                }
            }

            return parsedInput;
        }

        private List<(int, int, int)> GetAdjacent((int, int, int) coordinates) {
            int y = coordinates.Item1;
            int x = coordinates.Item2;

            // (y-1, x+1, y+1, x-1), if true, coordinates get emitted
            (bool, bool, bool, bool) directionMap = (
                    y != 0,
                    x != xMax, // heightMap.GetUpperBound(0)
                    y != yMax, // heightMap.GetUpperBound(1)
                    x != 0
                );
            // (y, x, height)
            List<(int, int, int)> adjacent = new List<(int, int, int)>();

            if (directionMap.Item1) { adjacent.Add((y - 1, x, heightMap[y - 1, x])); }
            if (directionMap.Item2) { adjacent.Add((y, x + 1, heightMap[y, x + 1])); }
            if (directionMap.Item3) { adjacent.Add((y + 1, x, heightMap[y + 1, x])); }
            if (directionMap.Item4) { adjacent.Add((y, x - 1, heightMap[y, x - 1])); }

            return adjacent;

        }

        private int Height(char mark) {
            return (int)mark - (int)'a';
        }
    }
}
