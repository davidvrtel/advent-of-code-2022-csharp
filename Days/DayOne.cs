using AdventOfCodeCSharp.Models;

namespace AdventOfCodeCSharp.Days {
    public class DayOne : Day {
        public DayOne(string nameOfDay, string pathToDataFile) : base(nameOfDay, pathToDataFile) {
            sortedCalories = SumAndSortCalories();
        }

        private int[] sortedCalories;

        private int[] SumAndSortCalories() {
            string[] elves = input.data.Split("\n\n");
            int[] elvesCalories = elves
                .Select(elf => elf.Split("\n"))
                .Select(elfCalories => elfCalories
                    .ToList()
                    .ConvertAll<int>(calories => {
                        int a = 0;
                        int.TryParse(calories, out a);
                        return a;
                    })
                    .Sum())
                .ToArray();

            Array.Sort(elvesCalories);

            return elvesCalories;
        }

        public override string PartOne() {
            return sortedCalories.Last().ToString();
        }

        public override string PartTwo() {
            return sortedCalories.TakeLast(3).Sum().ToString();
        }
    }
}
